---
title: 'ThreadX'
seo_title: 'ThreadX - Software Defined Vehicle'
keywords: ["special interest group", "software defined vehicle", "sig", "sdv", "ThreadX"]
headline: ThreadX - Software Defined Vehicle
summary: >-
  With ThreadX, we now have the first Real-Time Operating System (RTOS)
  certified for safety-critical applications. In addition, the maturity badges
  introduced by the SDV Working Group will help establish trust in open-source
  automotive projects by verifying and indicating the technology’s state of
  enterprise-readiness.
hide_page_title: true
hide_sidebar: true
page_css_file: public/css/threadx.css
container: "container"
header_wrapper_class: 'threadx-header-wrapper'
custom_jumbotron: |
      <div class="text-center">
        <h1>ThreadX</h1>
        <a class="btn btn-neutral" href="#join">Join this SIG</a>
      </div> 
---

{{< grid/div class="margin-bottom-10" >}}

## The First Open Source RTOS for Safety-Critical Applications { .h1 .margin-bottom-30 .text-center }

The [Eclipse ThreadX project](https://projects.eclipse.org/projects/iot.threadx), an embedded development suite including a Real-Time Operating System (RTOS), demonstrates that open source and full safety certification can form a successful symbiosis.
By fulfilling the highest automotive safety standards while embracing all the values and benefits of open source, ThreadX combines the best of both worlds.

In addition, its ultra-fast performance for resource-constrained devices makes it an ideal fit for the (Industrial) Internet of Things and the automotive industry.
Already deployed on more than 12 billion devices worldwide, ThreadX has proven itself in numerous real-world applications and use cases.
Originally dubbed Azure RTOS, the ThreadX project has been contributed to the Eclipse Foundation by Microsoft.

The Eclipse ThreadX Special Interest Group is your gateway to shaping the future of this exciting and powerful RTOS project.
Among other goals, the SIG will focus on:

* Supporting and maintaining ThreadX safety certifications
* Developing Eclipse engineering practices for safety certifications that may serve as a blueprint for other Eclipse SDV projects * looking to obtain safety certification
* Promoting Eclipse ThreadX as the premier open-source option for the automotive industry

[Read the full SIG proposal.](https://www.eclipse.org/lists/sdv-wg/msg00449.html)

Learn more about the project and find FAQs on the [ThreadX website](https://threadx.io/).

{{</ grid/div >}}

{{< grid/section-container id="leads" class="sig-leads">}}
  {{< events/user_display event="threadx" source="leads" imageRoot="images/" title="Lead" useCarousel="false" itemClass="col-md-offset-9 col-xs-12 col-xs-offset-6 col-sm-6 col-sm-offset-9 match-height-item-by-row">}}
  {{</ events/user_display >}}
{{</ grid/section-container >}}

{{< pages/special-interest-groups/members_list source="threadx" >}}

---

{{< pages/special-interest-groups/how_to_join >}}
