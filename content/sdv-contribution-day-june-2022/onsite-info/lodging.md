---
title: "Lodging"
date: 2022-08-10T13:09:34-04:00
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
#header_wrapper_class: ""
#seo_title: ""
#headline: ""
#subtitle: ""
#tagline: ""
#links: []
---
**Hotels in Friedrichshafen**

Seehotel – at the train station

Hotel City Krone – in the pedestrian area

Hotel Hellers Twenty Four I

Hotel Hellers Twenty Four II

Best Western Goldenes Rad – in the pedestrian area Lukullum Hotel

**Other hotels in the area**

Ringhotel Krone, Schnetzenhausen

Hotel Gerbe, Ailingen

Hotel Traube, Fischbach

Hotel Maier, Fischbach