---
title: "Information for Onsite Attendees"
seo_title: "Information for Onsite Attendees | SDV Contribution Day 2022"
date: 2022-06-01T08:00:00-24:00
hide_headline: true
hide_sidebar: true
container: "container text-center onsite-2022"
layout: "single"
resources:
    - src: "*.md"
---

{{< grid/div class="venue-photos margin-top-20 flex-center gap-20" isMarkdown="false">}}
<img src="./images/zf-forum-inside.jpg" alt="ZF Forum inside">
<img src="./images/zf-forum-day.jpg" alt="ZF Forum day">
{{</ grid/div >}} 

{{< info_list >}}
