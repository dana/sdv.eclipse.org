---
title: 'SDV Contribution Day'
headline: ''
custom_jumbotron: |
        <p class="sdv-title">Eclipse Software Defined Vehicle</p>
        <h1 class="event-title text-center">SDV Contribution Day - June 2022</h1>
        <p class="event-subtitle text-center">See how “code first” becomes operational</p>
        <div class="more-detail flex-center">
          <div class="more-detail-text">
            <p class="data-location">Hybrid Event | June 30, 2022</p> 
            <p>ZF Forum | Friedrichshafen, Germany</p>
          </div>
        </div>

date: 2022-02-02T08:00:00-24:00
hide_page_title: true
hide_headline: true
hide_sidebar: true
header_wrapper_class: 'header-sdv-contribution-day-2022'
hide_breadcrumb: true
container: 'container-fluid sdv-contribution-day-2022-container'
summary: ''
layout: single
---

<!-- Thank You -->
{{< grid/div class="container text-center" >}}
## Thank You!
The first SDV Contribution Day has ended. Thank you to our speakers, attendees, and working group members. Presentation recordings will be uploaded to the Eclipse Foundation YouTube channel soon.
{{</ grid/div >}}

<!-- About The Event -->
{{< grid/section-container id="registration" containerClass="padding-bottom-40 padding-top-40" >}}
  {{< grid/div class="container text-center" isMarkdown="false" >}}
    {{< events/registration event="sdv-contribution-day-june-2022" title="About the Event">}}
The first SDV Contribution Day will present the initial contributions for open source projects related to the newly established Eclipse Software Defined Vehicle (SDV) Working Group.

The event is open to any developer, product manager, architect, or business manager interested in topics related to the software defined vehicle.

The SDV Working Group mission is to foster collaboration across industries to create an open technology platform for the software defined vehicle of the future. The working group community has chosen a “code first” approach to facilitate more agile and faster time-to-market software development.
    {{</ events/registration >}}
  {{</ grid/div >}}
{{</ grid/section-container >}}

<!-- Speakers -->
{{< grid/section-container id="speakers" class="grey-row">}}
  {{< events/user_display event="sdv-contribution-day-june-2022" source="speakers" imageRoot="images/" title="Speakers">}}
{{</ grid/section-container >}}
{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}

<!-- Agenda -->
{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-light-bg" title="Agenda">}}
  {{< events/agenda event="sdv-contribution-day-june-2022" icon_class="fa-download">}}
{{</ grid/section-container >}}
{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}

<!-- Participating Member Companies -->
{{< grid/div class="container text-center padding-top-40 padding-bottom-30" isMarkdown="false">}}
    <h2>Thanks to Our Participating Member Companies</h2>
    <ul class="eclipsefdn-members-list list-inline margin-30 flex-center gap-40" data-ml-wg="sdv" data-ml-template="only-logos"></ul>
{{</ grid/div >}}

<!-- Hosted by -->
{{< grid/div class="row grey-row text-center padding-bottom-60 padding-top-40" isMarkdown="false">}}
  {{< grid/div class="container" >}}
## Hosted by
SDV Contribution Day is hosted by the Eclipse Software Defined Vehicle Working Group.
  {{</ grid/div >}}
{{</ grid/div >}}
