---
title: "Resources"
seo_title: "Resources - Software Defined Vehicle"
date: 2022-08-29T10:39:34-04:00
description: ""
keywords: ["Software Defined Vehicle", "SDV", "Newsroom", "Resources", "Reports", "Surveys", "Videos", "White Papers"]
slug: ""
aliases: []
container: 'container'
layout: single
hide_sidebar: false
---

Below are some useful links to articles and other resources about Eclipse SDV.

## Videos {.margin-top-60}

{{< youtube "playlist?list=PLy7t4z5SYNaRYI7lH26szREOi9pfqxOUU">}}

{{< grid/div class="margin-top-40 margin-bottom-60 text-center" isMarkdown="false" >}}
  <a class="btn btn-primary" href="./videos">View More</a>
{{</ grid/div >}}

---

## White Papers

{{< newsroom/resources wg="sdv" type="white_paper" template="cover" limit="3" viewMorePath="white-papers" >}}

