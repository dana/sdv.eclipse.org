complete: false
types:
    - name: Intro and Wrapup
      id: keynote
      color: "#eb6ebd"
    - name: Session
      id: session
      color: "#b2c5eb"
    - name: Break
      id: break
      color: "#000000"

items:
    - name: Welcome
      presenter: Nico Hartmann
      company: ZF Friedrichshafen AG
      type: keynote
      time: "13:30 - 13:40 (CEST)"

    - name: Introduction
      presenter: Stephen Walli
      company: Microsoft
      type: keynote
      time: 13:40 - 13:50 (CEST)

    - name: Key Principles of SDV
      presenter: Gunther Bauer
      company: SDV SC
      type: keynote
      time: 13:50 - 14:00 (CEST)
      slides: ZF_SDV_ContributionDay_0622.pdf

    - name: Eclipse eCAL
      presenter: Rex Schilasky
      company: Continental Automotive GmbH
      type: session
      time: 14:00 - 14:30 (CEST)
      slides: Eclipse_eCal_Continental_SDV_ContributionDay_0622.pdf
      abstract: |
        <p>The Eclipse eCAL (enhanced Communication Abstraction Layer) provides a middleware that enables scalable, high performance interprocess communication on a single computer node or between different nodes in a computer network. Eclipse eCAL uses a publish-subscribe pattern to automatically connect different nodes in the network.
        The middleware solution enables rapid prototyping for those high performance distributed meta-applications by providing the following base features:</p>
        <ul><li>intraprocess, interprocess and interhost communication</li>
        <li>different transport layer implementations (shared memory, UDP, TCP)</li>
        <li>different communication patterns as publish/subscribe and server/client</li>
        <li>builtin support for multiple standard message formats like google protobuf, flatbuffers or capnproto</li>
        <li>support for Windows as well as POSIX operating systems</li>
        <li>language bindings to C, C++, C#, Python, Rust, Go, M-Script</li>
        <li>powerful additional tools for live data monitoring, recording and replay</li></ul>
        <p>For more, see the <a href="https://continental.github.io/ecal/">official eCAL documentation</a>.</p>

    - name: Eclipse Chariott
      presenter: Filipe Prezado
      company: Microsoft
      type: session
      time: 14:30 - 15:00 (CEST)
      slides: Eclipse_Chariott_Microsoft_SDV_ContributionDay_0622.pdf
      abstract: |
        <p>The Eclipse Chariott project aims to develop a metadata-driven middleware/abstraction layer that allows modern programming models to target in-vehicle functions, providing a common way to access the vehicle hardware and sensors, while enhancing the developer journey.</p>

    - name: Eclipse SommR
      presenter: Gerd Schäfer and Tom Fleischmann
      company: Accenture GmbH, Cariad SE
      type: session
      time: 15:00 - 15:30 (CEST)
      slides: Eclipse_SommR_Cariad_SDV_ContributionDay_0622.pdf
      abstract: |
        <p>Eclipse SommR provides an automotive grade implementation of the someIP specification for embedded Linux systems together with the required tools to support developers.</p>

    - name: Eclipse ADAAA
      presenter: Andre Fischer
      company: ZF Friedrichshafen AG
      type: session
      time: 15:30 - 16:00 (CEST)
      slides: Eclipse_ADAAA_SDV_ContributionDay_0622.pdf
      abstract: |
        <p>ADAAA is example application for Adaptive AUTOSAR with the following goals:</p>
        <ul><li>Provide a tutorial with a simple example code base for getting involved with AUTOSAR methodology and each functional cluster</li>
        <li>Address one of the features available and known as a convenience function in modern cars: the Adaptive Cruise Control functionality</li>
        <li>Provide additional examples to illustrate how to put an AUTOSAR application into an automotive SW development context with its specific processes</li></ul>

    - name: Break
      presenter:
      type: break
      time: 16:00 - 16:30 (CEST)

    - name: Eclipse Velocitas
      presenter: Andreas Riexinger
      company: Robert Bosch GmbH
      type: session
      time: 16:30 - 17:00 (CEST)
      slides: Eclipse_Velocitas_Bosch_SDV_ContributionDay_0622.pdf
      abstract: |
        <p>Eclipse Velocitas provides an end-to-end, scalable, modular and open source development toolchain for creating containerized and non-containerized in-vehicle applications.</p>

    - name: Eclipse Leda
      presenter: Christian Heissenberger
      company: Robert Bosch GmbH
      type: session
      time: 17:00 - 17:30 (CEST)
      slides: Eclipse_Leda_Bosch_SDV_ContributionDay_0622.pdf
      abstract: |
        <p>In the ambition to build a technology ecosystem for software-defined vehicle, one of the main challenges will be the combination of the diverse components into a coherent and useful whole: all the software components in the world will not have the impact needed to transform the automotive industry unless we can make them play together coherently and form a functional portfolio.</p>

    - name: Eclipse Muto
      presenter: Naci Dai
      company: Eteration A.S.
      type: session
      time: 17:30 - 18:00 (CEST)
      slides: Eclipse_Muto_Eteration_SDV_ContributionDay_0622.pdf
      abstract: |
        <p>Eclipse Muto provides an adaptive framework and a runtime platform for dynamically composable model-driven ROS software stacks. Eclipse Muto can be used  to introspect, monitor and manipulate the actively running ROS graph (the network of ROS nodes).</p>

    - name: Project Ecosystem at the Eclipse Foundation
      presenter: Michael Plagge
      company: Eclipse Foundation
      type: session
      time: 18:00 - 18:30 (CEST)
      slides: Eclipse_Foundation_SDV_ContributionDay_220630.pdf

    - name: Outlook and Next Steps
      presenter: Ansgar Lindwedel
      company: SDV SC
      type: keynote
      time: 18:30 - 18:40 (CEST)

    - name: Visit to ZF Exhibition and Buffet Reception
      presenter: 
      type: break
      time: 18:40 - 21:15 (CEST)
